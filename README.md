# Description
This repository contains **two** test automation projects named `AutomationFramework` and `AutomationFrameworkTaskTwo` both are written in c# and require some dependencies to work properly. Complete list of dependencies are written at the last of this readme.
- Both the projects are independent of each other and can be run independently of each other.

### AutomationFramework
In this automation solution we do the Validation of user registration for the website using api, and verifying the registered user using user interface.
- Logging framework used is log4net
- Reporting framework used is Allure

### AutomationFrameworkTaskTwo
In this automation solution we test the consistency of book details between the backend API and the frontend UI.
- Logging framework used is log4net
- Reporting framework used is ExtentReports

<br>

>> Further Details can be seen in the respective feature files of both the projects, present in `*.Tests` directories.

# Dependencies

## Nuget packaged used
- Log4net (2.0.17)
- Allure.Specflow 
- Microsoft.NET.Test.SDK (17.0.0)
- Allure.NUnit
- Specflow.NUnit (3.9.40)
- Nunit (3.13.2)
- NUnit3TestAdapter (4.1.0)
- ExtentReports.Core (1.0.3)
- RestSharp (110.2.0)
- Selenium.Support (4.19.0)
- Selenium.WebDriver (4.19.0)

*For allure reports seperate installation need to be done and needs a server to run*
*`allure open` is the command needed to open the report*

## Tools used
- Visual Studio
- Postman
- Chrome browser
- Edge browser
- Git Bash

## Design Patterns Followed 
- Singleton
- Factory

## Frameworks used
- BDD - Specflow