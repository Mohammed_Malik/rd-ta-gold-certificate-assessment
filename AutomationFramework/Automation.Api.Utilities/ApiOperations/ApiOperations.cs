﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Api.Utilities.ApiOperations
{
    public class ApiOperations
    {
        readonly RestClient restClient;
        public ApiOperations(RestClient restClient)
        {
            this.restClient = restClient;
        }
        public RestResponse PostRequest<T>(T t) where T : class
        {
            var request = new RestRequest("", Method.Post);
            request.AddJsonBody(t);
            var response = restClient.Execute(request);
            Console.WriteLine(response.IsSuccessful + " post request done");
            return response;
        }
    }
}
