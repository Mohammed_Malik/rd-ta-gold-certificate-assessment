﻿using log4net;
using log4net.Config;

namespace Automation.Tests.StepDefinitions
{
    [Binding]
    internal class LoginValidationStepDefinitions
    {
        private readonly ScenarioContext _context;
        private IWebDriver? driver;
        private RestClient? client;
        private RestResponse? response;
        private static readonly ILog log = LogManager.GetLogger(typeof(ApiOperations));
        public LoginValidationStepDefinitions(ScenarioContext context)
        {
            _context = context;
            
        }
        [Given(@"I have a valid endpoint")]
        public void GivenIHaveAValidEndpoint()
        {
            client = _context.Get<RestClient>("RestClient");
        }
        [Given(@"I have the request body as ""([^""]*)"" as username and ""([^""]*)"" as password")]
        public void GivenIHaveTheRequestBodyAsAsUsernameAndAsPassword(string passtestdatauser, string passtestdatapwd)
        {
            XmlConfigurator.Configure(new FileInfo("Configurations/log4net.config"));
            log.Info("logging started");
            var requestData = new LoginCredentialsDto
            {
                userName = "passtestdatauser",
                password = "passtestdatapwd1A@"
            };
            string json = JsonConvert.SerializeObject(requestData, Formatting.Indented);
            File.WriteAllText("request.json", json);
        }
        [Given(@"I send a POST request to create a user with request body")]
        public void GivenISendAPOSTRequestToCreateAUserWithRequestBody()
        {
            
            var json = File.ReadAllText("request.json");
            var Usernames = System.Text.Json.JsonSerializer.Deserialize<LoginCredentialsDto>(json)?.userName;
            var Passwords = System.Text.Json.JsonSerializer.Deserialize<LoginCredentialsDto>(json)?.password;
            var requestData = new LoginCredentialsDto
            {
                userName = Usernames,
                password = Passwords
            };
            if (client == null)
            {
                log.Info("Client is null");
                return;
            }
            ApiOperations apiOperations = new(client);
            response = apiOperations.PostRequest(requestData);
            Console.WriteLine(response.Content);
        }



        [Then(@"I should receive a status code (.*)")]
        public void ThenIShouldReceiveAStatusCode(int p0)
        {
            if (response == null)
            {
                log.Error("response is null");
                return;
            }
            int StatusCode = (int)response.StatusCode;
            Console.WriteLine("The status code is "+ StatusCode);
            Assert.IsTrue(StatusCode == p0 || StatusCode == 406);
           
        }
        [Then(@"the response should contain a userid field")]
        public void ThenTheResponseShouldContainAUseridField()
        {
            if(response == null ) return;
            if (response.Content == null)
            {
                log.Error("response.Content is null");
                return;
            }
            Assert.IsTrue(response.Content.Contains("User exists!") || response.Content.Contains("userId"));
        }

        [Given(@"I navigate to the url")]
        public void GivenINavigateToTheUrl()
        {
            var json = File.ReadAllText("Configurations/AppSettings.json");
            var URL = System.Text.Json.JsonSerializer.Deserialize<AppSettingsDto>(json)?.UiUrl;
            driver = _context.Get<IWebDriver>("WebDriver");
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(URL);
        }

        [When(@"I enter username and password and login to the website")]
        public void WhenIEnterUsernameAndPasswordAndLoginToTheWebsite()
        {            
            var jsonRequest = File.ReadAllText("request.json");
            var Username = System.Text.Json.JsonSerializer.Deserialize<LoginCredentialsDto>(jsonRequest)?.userName;
            var Password = System.Text.Json.JsonSerializer.Deserialize<LoginCredentialsDto>(jsonRequest)?.password;
            if (driver == null)
            {
                log.Error("driver is null");
                return;
            }
            LoginPage loginPage = new(driver);
            if (Username == null || Password == null) return;
            loginPage.Login(Username, Password);
            Thread.Sleep(2000);
        }

        [Then(@"I should be redirected to the Books Dashboard")]
        public void ThenIShouldBeRedirectedToTheBooksDashboard()
        {
            if (driver == null)
            {
                log.Error("driver is null");
                return;
            }
            Assert.That("https://demoqa.com/profile", Is.EqualTo(driver.Url));
        }

        [Then(@"I should see the username ""([^""]*)"" on the page")]
        public void ThenIShouldSeeTheUsernameOnThePage(string passtestdatauser)
        {
            var jsonRequest = File.ReadAllText("request.json");
            var Username = System.Text.Json.JsonSerializer.Deserialize<LoginCredentialsDto>(jsonRequest)?.userName;
            if (driver == null)
            {
                log.Error("driver is null");
                return;
            }
            var text = driver.FindElement(By.XPath(ProfilePage.userNameDisplayLocator)).Text;
            Assert.That(Username,Is.EqualTo(text));
        }
    }
}
