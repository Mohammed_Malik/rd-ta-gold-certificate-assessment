﻿using Automation.Api.Utilities.Setup;

namespace Automation.Tests.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        private readonly ScenarioContext _context;

        public Hooks(ScenarioContext context)
        {
            _context = context;
        }
        public static AllureLifecycle allure = AllureLifecycle.Instance;
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            allure.CleanupResultDirectory();
        }
        [BeforeScenario("@UiTesting")]
        public void BeforeScenarioForUi()
        {
            var json = File.ReadAllText("Configurations/AppSettings.json");
            var appSettings = System.Text.Json.JsonSerializer.Deserialize<AppSettingsDto>(json);
            var browserName = appSettings?.BrowserName;
            if (browserName == null) return;
            _context.Set<IWebDriver>(Driver.GetDriver(browserName), "WebDriver");
        }

        [BeforeScenario("@ApiTesting")]
        public void BeforeScenarioForApi()
        {
            var json = File.ReadAllText("Configurations/AppSettings.json");
            var appSettings = System.Text.Json.JsonSerializer.Deserialize<AppSettingsDto>(json);
            var apiUrl = appSettings?.ApiUrl;
            if (apiUrl == null) return;
            _context.Set<RestClient>(Client.GetClient(apiUrl), "RestClient");
        }



        [AfterScenario("@ApiTesting")]
        public void AfterScenarioForUi()
        {
            _context.Get<RestClient>("RestClient").Dispose();
        }

        [AfterScenario("@UiTesting")]
        public void AfterScenarioForApi()
        {
            _context.Get<IWebDriver>("WebDriver").Dispose();
        }
    }
}