﻿namespace Automation.Tests.Support
{
    internal class AppSettingsDto
    {
        public string? BrowserName { get; set; }
        public string? ApiUrl { get; set; }  
        public string? UiUrl { get; set; }  
        
    }
}
