﻿namespace Automation.Tests.Support
{
    internal class LoginCredentialsDto
    {
        public string? userName { get; set; }
        public string? password { get; set; }
    }
}
