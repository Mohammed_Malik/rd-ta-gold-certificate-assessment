﻿Feature: Login Validation
Validation of user registration for the website using api, and verifying the registered user using user interface.

@ApiTesting
Scenario: Create User via API and Validate Response
	Given I have a valid endpoint
	And I have the request body as "passtestdatauser" as username and "passtestdatapwd" as password
	And I send a POST request to create a user with request body
	Then I should receive a status code 200
	And the response should contain a userid field

@UiTesting
Scenario: Login to Web UI with Credentials Created by API
	And I navigate to the url
	When I enter username and password and login to the website
	Then I should be redirected to the Books Dashboard
	And I should see the username "passtestdatauser" on the page
