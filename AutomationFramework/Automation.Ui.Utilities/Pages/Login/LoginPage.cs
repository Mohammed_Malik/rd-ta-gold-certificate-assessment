﻿using Automation.Ui.Utilities.Elements.Buttons;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Pages.Login
{
    public partial class LoginPage
    {
        private readonly IWebDriver driver;
        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;  
        }
        public void Login(string username, string password)
        {
            DataField dataField = new DataField(driver);
            dataField.SendText(loginLocator, username);
            dataField.SendText(passwordLocator, password);
            GeneralButton generalButton = new GeneralButton(driver);
            generalButton.Click(loginButtonLocator);
        }
    }
}
