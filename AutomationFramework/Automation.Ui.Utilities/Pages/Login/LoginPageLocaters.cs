﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Pages.Login
{
    public partial class LoginPage
    {
        public static readonly string loginLocator = "//*[@id=\"userName\"]";
        public static readonly string passwordLocator = "//*[@id=\"password\"]";
        public static readonly string loginButtonLocator = "//*[@id=\"login\"]";

    }
}
