﻿using Automation.Ui.Utilities.Setup;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Waits
{
    public class GeneralWait
    {
        public static bool WaitForElement(IWebDriver driver, string xpath)
        {
            try
            {

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                wait.Until(drv => drv.FindElement(By.XPath(xpath)));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
