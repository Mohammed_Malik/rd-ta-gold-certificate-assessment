﻿using Automation.Ui.Utilities.Waits;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Elements.Buttons
{
    public class DataField
    {
        private readonly IWebDriver driver;
        public DataField(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void SendText(string xpath, string text)
        {
            if (GeneralWait.WaitForElement(driver, xpath))
            {
                var element = driver.FindElement(By.XPath(xpath));
                element.SendKeys(text);
            }
            else
            {
                Console.WriteLine("Field not found");
            }
        }
    }
}
