﻿using Automation.Ui.Utilities.Waits;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Elements.Buttons
{
    internal class GeneralButton
    {
        private readonly IWebDriver driver;
        public GeneralButton(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void Click(string xpath)
        {

            if(GeneralWait.WaitForElement(driver,xpath))
            {
                var element = driver.FindElement(By.XPath(xpath));
                var js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
                element.Click();
            }
            else
            {
                Console.WriteLine("ELement not found");
            }
        }
    }
}
