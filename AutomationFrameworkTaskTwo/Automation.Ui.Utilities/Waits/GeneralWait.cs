﻿using Automation.Ui.Utilities.Setup;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Waits
{
    public class GeneralWait
    {
        protected GeneralWait() { }
        public static bool WaitForElement(IWebDriver driver, string xpath)
        {
            try
            {

                WebDriverWait wait = new(driver, TimeSpan.FromSeconds(2));
                wait.Until(drv => drv.FindElement(By.XPath(xpath)));
                return true;
            }
            catch (Exception ex)
            {
                throw new ElementNotVisibleException(ex.Message);
            }
        }
    }
}
