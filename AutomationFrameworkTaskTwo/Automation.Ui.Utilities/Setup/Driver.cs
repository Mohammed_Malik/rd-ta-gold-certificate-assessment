﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Setup
{
    public class Driver
    {
        private Driver() { }

        public static IWebDriver GetDriver(string browserName)
        {
                if (browserName == "Chrome")
                    return new ChromeDriver();
                if (browserName == "Edge")
                    return new EdgeDriver();
                else
                    return new ChromeDriver();
        }

    }
}
