﻿using Automation.Ui.Utilities.Waits;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Pages.Book
{
    public class BooksPage
    {
        private readonly IWebDriver driver;
        public BooksPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public List<BooksBasicDto> GetDetails()
        {
            string titlesXpath = $"//div/div/div[2]/div/span/a";
            string authorsXpath = $"//div[2]/div[1]/div[2]/div/div/div[3]";
            string publishersXpath = $"//div[2]/div[2]/div[1]/div[2]/div/div/div[4]";
            if (GeneralWait.WaitForElement(driver, titlesXpath))
            {

                var titles = driver.FindElements(By.XPath(titlesXpath));
                var authors = driver.FindElements(By.XPath(authorsXpath));
                var publishers = driver.FindElements(By.XPath(publishersXpath));

                var books = new List<BooksBasicDto>();
                for (int i = 0; i < titles.Count; i++)
                {
                    var book = new BooksBasicDto
                    {
                        title = titles[i].Text,
                        author = authors[i].Text,
                        publisher = publishers[i].Text
                    };
                    books.Add(book);
                }
                return books;
            }
            else
            {
                throw new ElementNotVisibleException();
            }
        }
    }
}
