﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Ui.Utilities.Pages.Book;

public class BooksBasicDto
{
    public string? title { get; set; }
    public string? author { get; set; }
    public string? publisher { get; set; }
}
