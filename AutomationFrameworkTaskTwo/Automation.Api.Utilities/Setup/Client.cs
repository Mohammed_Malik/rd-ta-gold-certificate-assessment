﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Api.Utilities.Setup
{
    public class Client
    {
        public static RestClient client;

        private Client() { }

        public static RestClient GetClient(string url)
        {
            if (client == null)
            {
                client = new RestClient(url);
                return client;
            }
            else
                return client;
        }

    }
}
