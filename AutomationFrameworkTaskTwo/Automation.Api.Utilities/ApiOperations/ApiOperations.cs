﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Automation.Api.Utilities.ApiOperations
{
    public class ApiOperations
    {
        readonly RestClient restClient;
        public ApiOperations(RestClient restClient)
        {
            this.restClient = restClient;
        }
        public RestResponse GetRequest(string path)
        {
            var request = new RestRequest(path, Method.Get);
            var response = restClient.Execute(request, Method.Get);
            return response;
        }
    }
}
