﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Tests.Support
{
    public class BookResponseDto
    {
        public List<BookDto>? Books { get; set; }
    }
}
