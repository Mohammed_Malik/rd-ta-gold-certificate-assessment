﻿Feature: Validate Book Details
This feature tests the consistency of book details between the backend API and the frontend UI.
It ensures that the details fetched from the API match those displayed on the website.
The API is used to fetch a list of books and their details, which are then compared against
the information displayed on the web page.

Scenario: Verify book details through API and Web UI
	When I make a GET request to the API endpoint
	And I capture the Title, Author, and Publisher of each book from the response
	Given I launch the URL in a browser
	Then the web page should display the list of all books
	Then I validate that each book's Title, Author, and Publisher match between the API response and the web UI
