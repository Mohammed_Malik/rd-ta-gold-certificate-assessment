﻿using log4net;
using log4net.Config;

namespace Automation.Tests.StepDefinitions
{
    [Binding]
    internal class ValidateBookDetailsStepDefinitions
    {
        private readonly ScenarioContext _context;
        private readonly IWebDriver driver;
        private readonly RestClient client;
        private ApiOperations? apiOperations;
        private static readonly ILog log = LogManager.GetLogger(typeof(ApiOperations));
        public ValidateBookDetailsStepDefinitions(ScenarioContext context)
        {
            _context = context;
            client = _context.Get<RestClient>("RestClient");
            driver = _context.Get<IWebDriver>("WebDriver");
        }


        [When(@"I make a GET request to the API endpoint")]
        public void WhenIMakeAGETRequestToTheAPIEndpoint()
        {
            XmlConfigurator.Configure(new FileInfo("Configurations/log4net.config"));
            log.Info("logging started");
            apiOperations = new ApiOperations(client);
        }

        [When(@"I capture the Title, Author, and Publisher of each book from the response")]
        public void WhenICaptureTheTitleAuthorAndPublisherOfEachBookFromTheResponse()
        {
            if (apiOperations == null)
            {
                log.Error("apiOperations is null");
                return;
            }
            var response = apiOperations.GetRequest("");
            if(response.Content == null) return;
            BookResponseDto? booksResponse = JsonConvert.DeserializeObject<BookResponseDto>(response.Content);
            if (booksResponse == null) return;
            _context.Set<BookResponseDto>(booksResponse, "booksFromApi");
        }
        [Given(@"I launch the URL in a browser")]
        public void GivenILaunchTheURLInABrowser()
        {
            var json = File.ReadAllText("Configurations/AppSettings.json");
            var URL = System.Text.Json.JsonSerializer.Deserialize<AppSettingsDto>(json)?.UiUrl;

            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(URL);
        }

        [Then(@"the web page should display the list of all books")]
        public void ThenTheWebPageShouldDisplayTheListOfAllBooks()
        {
            BooksPage booksPage = new(driver);
            List<BooksBasicDto> booksFromUi = booksPage.GetDetails();
            _context.Set<List<BooksBasicDto>>(booksFromUi, "booksFromUi");
        }

        [Then(@"I validate that each book's Title, Author, and Publisher match between the API response and the web UI")]
        public void ThenIValidateThatEachBooksTitleAuthorAndPublisherMatchBetweenTheAPIResponseAndTheWebUI()
        {
            var booksFromUi = _context.Get<List<BooksBasicDto>>("booksFromUi");
            var booksFromApi = _context.Get<BookResponseDto>("booksFromApi");
            if (booksFromApi.Books != null)
            {
                foreach (var bookFromApi in booksFromApi.Books)
                {

                    foreach (var bookFromUi in booksFromUi)
                    {
                        if (bookFromApi.title == bookFromUi.title)
                        {
                            Assert.True(bookFromApi.author == bookFromUi.author);
                            Assert.True(bookFromApi.publisher == bookFromUi.publisher);
                        }

                    }
                }
            }
            else
            {
                log.Info("Books are null");
                Assert.Fail();
            }
        }
    }
}
