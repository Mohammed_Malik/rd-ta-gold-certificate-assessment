﻿

namespace Automation.Tests.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        private readonly ScenarioContext _context;

        public Hooks(ScenarioContext context)
        {
            _context = context;
        }
        private static AventStack.ExtentReports.ExtentReports? extent;
        private static AventStack.ExtentReports.ExtentTest? feature;
        private static AventStack.ExtentReports.ExtentTest? scenario;
        private static AventStack.ExtentReports.ExtentTest? step;
        static string? reportPath = System.IO.Directory.GetParent(@"../../../")?.FullName
            + Path.DirectorySeparatorChar + "Result"
            + Path.DirectorySeparatorChar + "Result_" + DateTime.Now.ToString("ddMMyyyy HHmmss");

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            var htmlReporter = new ExtentHtmlReporter(reportPath);
            extent = new AventStack.ExtentReports.ExtentReports();
            extent.AttachReporter(htmlReporter);
        }
        [BeforeFeature]
        public static void BeforeFeature(FeatureContext context)
        {
            if (extent == null) return;
            feature = extent.CreateTest(context.FeatureInfo.Title);

        }
        [BeforeStep]
        public void BeforeTest()
        {
            step = scenario;
        }
        [AfterStep]
        public void AfterTest(ScenarioContext context)
        {
            if (context.TestError == null)
            {
                if (step == null) return;
                step.Log(Status.Pass, context.StepContext.StepInfo.Text);
            }
            else if (context.TestError != null)
            {
                if (step == null) return;
                step.Log(Status.Fail, context.StepContext.StepInfo.Text);
            }
        }
        [AfterFeature]
        public static void AfterFeature()
        {
            if (extent == null) return;
            extent.Flush();
        }
        [BeforeScenario]
        public void BeforeScenarioForApi(ScenarioContext _context)
        {
            var json = File.ReadAllText("Configurations/AppSettings.json");
            var appSettings = System.Text.Json.JsonSerializer.Deserialize<AppSettingsDto>(json);
            if (appSettings != null)
            {
                var BaseUrl = appSettings.ApiUrl;
                if (BaseUrl == null) { return; }
                _context.Set<RestClient>(Client.GetClient(BaseUrl), "RestClient");

                var browserName = appSettings.BrowserName;
                if (browserName == null) { return; }
                _context.Set<IWebDriver>(Driver.GetDriver(browserName), "WebDriver");
                if (feature == null) return;
                scenario = feature.CreateNode(_context.ScenarioInfo.Title);
            }
        }
        [AfterScenario]
        public void AfterScenarioForApi()
        {
            _context.Get<RestClient>("RestClient").Dispose();
            _context.Get<IWebDriver>("WebDriver").Dispose();
        }
    }
}